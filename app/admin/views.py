from flask import (
    Blueprint,
    abort,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_login import current_user, login_required
from flask_rq import get_queue

from app import db
from app.admin.forms import (
    ChangeAccountTypeForm,
    ChangeUserEmailForm,
    InviteUserForm,
    NewUserForm,
    AnswerPollForm,
    ManageKiosqueForm,
    ParticipantsForm
)
from app.decorators import admin_required
from app.email import send_email
from app.models import EditableHTML, Role, User, Kiosques, Participants
import pdb
admin = Blueprint('admin', __name__)


@admin.route('/')
@login_required
@admin_required
def index():
    """Admin dashboard page."""
    return render_template('admin/index.html')


@admin.route('/new-user', methods=['GET', 'POST'])
@login_required
@admin_required
def new_user():
    """Create a new user."""
    form = NewUserForm()
    if form.validate_on_submit():
        user = User(
            role=form.role.data,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            email=form.email.data,
            truite=form.truite.data,
            pintade=form.pintade.data,
            password=form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('User {} successfully created'.format(user.full_name()),
              'form-success')
    return render_template('admin/new_user.html', form=form)


@admin.route('/invite-user', methods=['GET', 'POST'])
@login_required
@admin_required
def invite_user():
    """Invites a new user to create an account and set their own password."""
    form = InviteUserForm()
    if form.validate_on_submit():
        user = User(
            role=form.role.data,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            email=form.email.data)
        db.session.add(user)
        db.session.commit()
        token = user.generate_confirmation_token()
        invite_link = url_for(
            'account.join_from_invite',
            user_id=user.id,
            token=token,
            _external=True)
        get_queue().enqueue(
            send_email,
            recipient=user.email,
            subject='You Are Invited To Join',
            template='account/email/invite',
            user=user,
            invite_link=invite_link,
        )
        flash('User {} successfully invited'.format(user.full_name()),
              'form-success')
    return render_template('admin/new_user.html', form=form)


@admin.route('/users')
@login_required
@admin_required
def registered_users():
    """View all registered users."""
    users = User.query.all()
    roles = Role.query.all()
    return render_template(
        'admin/registered_users.html', users=users, roles=roles)


@admin.route('/user/<int:user_id>')
@admin.route('/user/<int:user_id>/info')
@login_required
@admin_required
def user_info(user_id):
    """View a user's profile."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    return render_template('admin/manage_user.html', user=user)


@admin.route('/user/<int:user_id>/change-email', methods=['GET', 'POST'])
@login_required
@admin_required
def change_user_email(user_id):
    """Change a user's email."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    form = ChangeUserEmailForm()
    if form.validate_on_submit():
        user.email = form.email.data
        db.session.add(user)
        db.session.commit()
        flash('Email for user {} successfully changed to {}.'.format(
            user.full_name(), user.email), 'form-success')
    return render_template('admin/manage_user.html', user=user, form=form)


@admin.route(
    '/user/<int:user_id>/change-account-type', methods=['GET', 'POST'])
@login_required
@admin_required
def change_account_type(user_id):
    """Change a user's account type."""
    if current_user.id == user_id:
        flash('You cannot change the type of your own account. Please ask '
              'another administrator to do this.', 'error')
        return redirect(url_for('admin.user_info', user_id=user_id))

    user = User.query.get(user_id)
    if user is None:
        abort(404)
    form = ChangeAccountTypeForm()
    if form.validate_on_submit():
        user.role = form.role.data
        db.session.add(user)
        db.session.commit()
        flash('Role for user {} successfully changed to {}.'.format(
            user.full_name(), user.role.name), 'form-success')
    return render_template('admin/manage_user.html', user=user, form=form)


@admin.route('/user/<int:user_id>/delete')
@login_required
@admin_required
def delete_user_request(user_id):
    """Request deletion of a user's account."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    return render_template('admin/manage_user.html', user=user)


@admin.route('/user/<int:user_id>/_delete')
@login_required
@admin_required
def delete_user(user_id):
    """Delete a user's account."""
    if current_user.id == user_id:
        flash('You cannot delete your own account. Please ask another '
              'administrator to do this.', 'error')
    else:
        user = User.query.filter_by(id=user_id).first()
        db.session.delete(user)
        db.session.commit()
        flash('Successfully deleted user %s.' % user.full_name(), 'success')
    return redirect(url_for('admin.registered_users'))


@admin.route('/_update_editor_contents', methods=['POST'])
@login_required
@admin_required
def update_editor_contents():
    """Update the contents of an editor."""

    edit_data = request.form.get('edit_data')
    editor_name = request.form.get('editor_name')

    editor_contents = EditableHTML.query.filter_by(
        editor_name=editor_name).first()
    if editor_contents is None:
        editor_contents = EditableHTML(editor_name=editor_name)
    editor_contents.value = edit_data

    db.session.add(editor_contents)
    db.session.commit()

    return 'OK', 200
from flask import request


@admin.route('/answer_poll', methods=['POST','GET'])
def answer_poll():
    """Invites a new user to create an account and set their own password."""
    form = AnswerPollForm()
    form.validate_email(form.email)
    kiosque_1_text=""
    kiosque_2_text=""
    kiosque_3_text=""
    kiosque_4_text=""
    kiosque_5_text=""

    if form.validate_on_submit() & form.submit.data == True:
        participant = Participants(
            email=form.email.data,
            kiosque_1=form.kiosque_1.data.sub_name,
            kiosque_2=form.kiosque_2.data.sub_name,
            kiosque_3=form.kiosque_3.data.sub_name,
            kiosque_4=form.kiosque_4.data.sub_name,
            kiosque_5=form.kiosque_5.data.sub_name,
            )
        def create_place_names(name,sub_name,places):
             return name +': '+sub_name+" - "+ str(places)+" places disponibles"
        doublon = Participants.query.filter_by(email=form.email.data).first()
        if doublon :
            for doublon_n in [doublon.kiosque_1,doublon.kiosque_2,doublon.kiosque_3,doublon.kiosque_4,doublon.kiosque_5]:
                kiosque_n = Kiosques.query.filter_by(sub_name = doublon_n).first()    
                kiosque_n.places = kiosque_n.places + 1
                kiosque_n.name_places = create_place_names(kiosque_n.name,kiosque_n.sub_name,kiosque_n.places)
                db.session.add(kiosque_n)

            doublon.kiosque_1=form.kiosque_1.data.sub_name
            doublon.kiosque_2=form.kiosque_2.data.sub_name
            doublon.kiosque_3=form.kiosque_3.data.sub_name
            doublon.kiosque_4=form.kiosque_4.data.sub_name
            doublon.kiosque_5=form.kiosque_5.data.sub_name
            db.session.add(doublon)
        else:        
            db.session.add(participant)
        for kiosque_n in ["kiosque_1","kiosque_2","kiosque_3","kiosque_4","kiosque_5"]:
            kiosque_n = Kiosques.query.filter_by(name_places = form[kiosque_n].data.name_places).first()    
            kiosque_n.places = kiosque_n.places - 1
            #if kiosque_n.places == 0:
                #raise AttributeError("Plus de places")
            kiosque_n.name_places = create_place_names(kiosque_n.name,kiosque_n.sub_name,kiosque_n.places)
            db.session.add(kiosque_n)
        flash('Merci {}, tes choix ont bien été pris en compte'.format(participant.email),'form-success')

    if form.submit_2.data == True :
        doublon = Participants.query.filter_by(email=form.email.data).first()
        if doublon :
            kiosque_1_text =  doublon.kiosque_1
            kiosque_2_text =  doublon.kiosque_2
            kiosque_3_text =  doublon.kiosque_3
            kiosque_4_text =  doublon.kiosque_4
            kiosque_5_text =  doublon.kiosque_5
            participant = Participants(
            email=form.email.data,
            kiosque_1=form.kiosque_1,
            kiosque_2=form.kiosque_2,
            kiosque_3=form.kiosque_3,
            kiosque_4=form.kiosque_4,
            kiosque_5=form.kiosque_5,
            )

        print(kiosque_1_text)
        if kiosque_1_text != "":
            flash('Voici tes choix','form-success')
        else :
            flash("Tu n'as pas encore fait de choix",'form-error')

        
        


        db.session.commit()


    
        
    return render_template('admin/poll.html', form=form,kiosque_1_text=kiosque_1_text,kiosque_2_text=kiosque_2_text,kiosque_3_text=kiosque_3_text,kiosque_4_text=kiosque_4_text,kiosque_5_text=kiosque_5_text)

from flask import Response
import pandas as pd    
@admin.route('/participants', methods=['POST','GET'])
@login_required
@admin_required
def participants():
    form = ParticipantsForm()
    participants = Participants.query.all()
    
    """View all registered users."""
    def create_place_names(name,sub_name,places):
             return name +': '+sub_name+" - "+ str(places)+" places disponibles"
    

    if form.validate_on_submit():
        
        participant_to_delete = Participants.query.filter_by(email = form.participants_all.data.email).first()

        def re_add_places_after_suppression(kiosque_n):
            kiosque_n.places = kiosque_n.places + 1
            kiosque_n.name_places = create_place_names(kiosque_n.name,kiosque_n.sub_name,kiosque_n.places)
            db.session.add(kiosque_n)
            db.session.commit()

        kiosque_n = Kiosques.query.filter_by(sub_name = form.participants_all.data.kiosque_1).first()    
        re_add_places_after_suppression(kiosque_n)
        kiosque_n = Kiosques.query.filter_by(sub_name = form.participants_all.data.kiosque_2).first()    
        re_add_places_after_suppression(kiosque_n)
        kiosque_n = Kiosques.query.filter_by(sub_name = form.participants_all.data.kiosque_3).first()    
        re_add_places_after_suppression(kiosque_n)
        kiosque_n = Kiosques.query.filter_by(sub_name = form.participants_all.data.kiosque_4).first()    
        re_add_places_after_suppression(kiosque_n)
        kiosque_n = Kiosques.query.filter_by(sub_name = form.participants_all.data.kiosque_5).first()    
        re_add_places_after_suppression(kiosque_n)

        db.session.delete(participant_to_delete)
        db.session.commit()
        flash('Participant supprimé')
        participants = Participants.query.all()
        
    
    return render_template(
        'admin/participants.html', users=participants, form=form)

@admin.route("/getPlotCSV")
def getPlotCSV():
    participants = Participants.query.all()
    list_participants=[]
    for i in range(len(participants)):
        parti=participants[i].__dict__
        list_participants.append([parti["email"],parti["kiosque_1"],parti["kiosque_2"],parti["kiosque_3"],parti["kiosque_4"],parti["kiosque_5"]])
    print(list_participants)
    DF_participants=pd.DataFrame(list_participants)
    csv = DF_participants.to_csv(header=False,index=False)
    return Response(
        csv,
        mimetype="text/csv",
        headers={"Content-disposition":
                 "attachment; filename=Liste_Metapleniere.csv"})

@admin.route('/manage_kiosques', methods=['POST','GET'])
@login_required
@admin_required
def manage_kiosques():
    form = ManageKiosqueForm()
    kiosques = Kiosques.query.all()
    #pdb.set_trace()
    def create_place_names(name,sub_name,places):
             return name +': '+sub_name+" - "+ str(places)+" places disponibles"
    

    if form.validate_on_submit():
        

        old_title=form.kiosques_all.data.sub_name
        data_to_change=Kiosques.query.filter_by(sub_name = old_title).first()
        if (form.new_name.data!=None) & (form.new_name.data!=""):
            data_to_change.sub_name=form.new_name.data        
        new_number_places=form.places_disponibles.data
        old_number_places_max=data_to_change.places_max
        data_to_change.places_max=new_number_places
        data_to_change.places = data_to_change.places+(new_number_places-old_number_places_max)
        data_to_change.name_places = create_place_names(data_to_change.name,data_to_change.sub_name,data_to_change.places)
        db.session.add(data_to_change)
        
        db.session.commit()
        flash('Sous_titre changé')

          
    return render_template(
        'admin/manage_kiosques.html', users=kiosques, form=form)