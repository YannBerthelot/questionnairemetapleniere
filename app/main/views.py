from flask import Blueprint, render_template

from app.models import EditableHTML
from app.models import Participants

main = Blueprint('main', __name__)


@main.route('/')
def index():
    return render_template('main/index.html')


@main.route('/poll', methods=['GET', 'POST'])
def about():
    editable_html_obj = EditableHTML.get_editable_html('poll')
    return render_template(
        'main/poll.html', editable_html_obj=editable_html_obj)
