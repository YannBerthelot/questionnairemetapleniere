heroku config:set FLASK_CONFIG=production
heroku config:set SSL_DISABLE=False
heroku ps:scale web=1 worker=1
heroku config:set SECRET_KEY=3259d37567cdd401c529beb250444417
heroku run python manage.py recreate_db
heroku run python manage.py setup_dev
heroku run python manage.py setup_kiosque
